<?php

namespace app\controllers;

use Yii;
use ns\rest\RestFullController;
use ns\rest\RestFullTreeController;

/* 
	Все категории заявок
*/
class TaskscategorysController extends RestFullTreeController
{
    public $modelClass = '\app\models\TasksCategorys';
}
<?php

namespace ns\rest;

use Yii;
 
class ApiErrorHandler extends \yii\web\ErrorHandler
{
 
    /**
     * @inheridoc
     */
 
    protected function renderException($exception)
    {
        /*
        if (Yii::$app->has('response')) {
            $response = Yii::$app->getResponse();
        } else {
            $response = new Response();
        }
        */

        $response = Yii::$app->getResponse();
        //$response->format = yii\web\Response::FORMAT_JSON;
        $response->data = [
            'name' => $exception->getName(),
            'message' => $exception->getMessage(),
            'code' => $exception->getCode(),
            'status' => $exception->statusCode,
            'type' => get_class($exception),
        ];
        $response->setStatusCode($exception->statusCode);
        $response->send();
    }

}
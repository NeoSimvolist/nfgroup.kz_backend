<?php

namespace ns\rest;

use Yii;
use yii\data\ActiveDataProvider;
use yii\rest\Action;

class IndexAction extends Action
{
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }
        $modelClass = $this->modelClass;
        $model = new $modelClass();
        return Yii::createObject([
            'class' => ActiveDataProvider::className(),
            'query' => $modelClass::find()->where([$model->parentAttribute => null]),
        ]);
    }
}
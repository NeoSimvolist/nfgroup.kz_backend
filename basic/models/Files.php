<?php

namespace app\models;

use Yii;
use ns\NsHelper;

/**
 * This is the model class for table "files".
 *
 * @property integer $fl_id
 * @property string $fl_path
 * @property string $fl_name
 * @property string $fl_ext
 * @property string $fl_created
 * @property integer $fl_size
 */
class Files extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'files';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //fl_created
            ['fl_created', 'safe'],
            ['fl_created', 'default', 'value' => (new \DateTime)->format('Y-m-d H:i:s')],
            [['fl_size'], 'integer'],
            [['fl_path', 'fl_name'], 'string', 'max' => 255],
            [['fl_ext'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fl_id' => Yii::t('app', 'Fl ID'),
            'fl_path' => Yii::t('app', 'Fl Path'),
            'fl_name' => Yii::t('app', 'Fl Name'),
            'fl_ext' => Yii::t('app', 'Fl Ext'),
            'fl_created' => Yii::t('app', 'Fl Created'),
            'fl_size' => Yii::t('app', 'Fl Size'),
        ];
    }

    public function allowExtension() {
        return ['jpg','png','jpeg'];
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $r = NsHelper::upload(null, [
                'uploadDir' => 'uploads',
                'extensions' => $this->allowExtension(),
            ]);
            if (isset($r['filename'])) {
                $this->fl_path = $r['filename'];
                $this->fl_name = $r['name'];
                $this->fl_ext = $r['extension'];
                $this->fl_size = $r['size'];
            } else {
                $this->addError($r['field'], $r['error']);    
            }
        }
        return parent::beforeSave($insert);
    }
}

<?php

namespace app\controllers;

use Yii;
use ns\rest\RestFullController;
use yii\web\NotFoundHttpException;

/* 
	Все категории заявок
*/
class WebpushController extends RestFullController
{
    public $modelClass = '\app\models\Webpush';


   	public function actionBytoken()
    {
    	$modelClass = $this->modelClass;
    	$token = Yii::$app->request->get('token'); 
    	$model = $modelClass::findOne(['wbp_token' => $token, 'wbp_user_id' => Yii::$app->user->identity->us_id]);
        if (isset($model)) {
            return $model;
        } else {
            throw new NotFoundHttpException("Object not found: $token");
        }
    }

    public function actionBytokendelete()
    {
        $modelClass = $this->modelClass;
        $token = Yii::$app->request->getBodyParam('token');
        return $modelClass::deleteAll(['wbp_token' => $token]);
        Yii::$app->getResponse()->setStatusCode(204);
    }

    public function prepareRules() {
        $rules = parent::prepareRules();
        $rules['bytoken'] = ['roles' => ['author']];
        $rules['bytokendelete'] = ['roles' => ['author']];
        unset($rules['index']);
        unset($rules['update']);
        unset($rules['view']);
        unset($rules['delete']);
        return $rules;
    }


}
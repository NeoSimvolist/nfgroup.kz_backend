<?php

namespace app\controllers;

use Yii;
use ns\rest\RestFullController;
use app\models\Files;
use yii\helpers\ArrayHelper;

class FilesController extends RestFullController
{
    public $modelClass = 'app\models\Files';
}

<?php

namespace app\models;

use Yii;

class MySubscriptions extends Subscriptions
{

    /*
    !! некорректно работает с rule unique (см. ниже)
    public function behaviors()
    {
        return [
            // Автор записи
            [
                'class' => \yii\behaviors\BlameableBehavior::className(),
                'createdByAttribute' => 'sbs_user_id',
                'updatedByAttribute' => false,
            ],
        ];
    }
    */

    public function rules()
    {
        return [
            [['sbs_user_id'], 'default', 'value'=> Yii::$app->user->identity->us_id],
            [['sbs_user_id', 'sbs_category_id'], 'required'],
            [['sbs_user_id', 'sbs_category_id'], 'integer'],
            ['sbs_category_id', 'unique', 'targetAttribute' => ['sbs_user_id', 'sbs_category_id'], 'message' => 'Вы уже подписаны на эту тему'],
        ];
    }

	// Только текущего пользователя
    public static function find()
    {   
        return parent::find()->andWhere(['sbs_user_id' => Yii::$app->user->identity->us_id]);
    }

    public function fields()
    {
        $fields = parent::fields();
        // Добавляем поля в выдачу
        $fields['sbs_category_name'] = function() {
            if (!isset($this->category)) return null;
            return $this->category->tskct_name;
        };
        return $fields;
    }

    /* 
        Связь с TasksCategorys / Категории
    */
    public function getCategory()
    {
        return $this->hasOne(TasksCategorys::className(), ['tskct_id' => 'sbs_category_id']);
    }
}

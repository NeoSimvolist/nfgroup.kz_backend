<?php

namespace app\models;

use Yii;
use app\commands\RabbitController;

/**
 * This is the model class for table "solutions".
 *
 * @property integer $sl_id
 * @property string $sl_text
 * @property integer $sl_task_id
 * @property string $sl_created
 * @property integer $sl_author_id
 * @property integer $sl_updated
 * @property integer $sl_status_id
 */
class Solutions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'solutions';
    }

    public function behaviors()
    {
        return [
            // Дата создания и последнего редактирования записи
            [
                'class' => \ns\behaviors\DatetimeBehavior::className(),
                'createdAtAttribute' => 'sl_created',
                'updatedAtAttribute' => 'sl_updated',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sl_text', 'sl_task_id'], 'required'],
            [['sl_text'], 'string'],
            [['sl_task_id', 'sl_status_id'], 'integer'],
            ['sl_author_id', 'default', 'value'=> Yii::$app->user->identity->us_id],
            //['sl_text', 'unique', 'targetAttribute' => ['sl_task_id', 'sl_author_id'], 'message' => 'Твой ответ уже принят! Не нужно спамить'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sl_id' => Yii::t('app', 'Sl ID'),
            'sl_text' => Yii::t('app', 'Sl Text'),
            'sl_task_id' => Yii::t('app', 'Sl Task ID'),
            'sl_created' => Yii::t('app', 'Sl Created'),
            'sl_author_id' => Yii::t('app', 'Sl Author ID'),
            'sl_updated' => Yii::t('app', 'Sl Updated'),
            'sl_status_id' => Yii::t('app', 'Sl Status ID'),
        ];
    }

    /* 
        Связь с Tasks
    */
    public function getTask()
    {
        return $this->hasOne(Tasks::className(), ['tsk_id' => 'sl_task_id']);
    }

    public function afterSave( $insert, $changedAttributes ) {
        parent::afterSave($insert, $changedAttributes);
        if ($insert == true) {
            // Новый ответ
            RabbitController::sendDispatch([
                'scenario' => 'newsolution',
                'task_id' => $this->sl_task_id,
                'user_id' => isset($this->task) ? $this->task->tsk_author_id : null,
                'solution_id' => $this->sl_id,
                'message' => $this->sl_text, // !текст
            ]);
        }
    }
}

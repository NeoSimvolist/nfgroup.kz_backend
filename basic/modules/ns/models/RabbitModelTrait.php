<?php

namespace ns\models;

use Yii;
use app\commands\RabbitController;

trait RabbitModelTrait
{

  public function afterSave( $insert, $changedAttributes ) {
    parent::afterSave($insert, $changedAttributes);
    RabbitController::websocket([
    	'event' => 'websocket',
    	'model' => Self::tableName(),
    	'action' => ($insert == true) ? 'create' : 'update',
    	'data' => $this->toArray(),
    	'key' => $this->getPrimaryKey(),
   	]);
  }

  public function afterDelete()
  {
		parent::afterDelete();
    RabbitController::websocket([
    	'event' => 'websocket',
    	'model' => Self::tableName(),
    	'action' => 'delete',
    	'data' => $this->toArray(),
    	'key' => $this->getPrimaryKey(),
   	]);
  }
    
}
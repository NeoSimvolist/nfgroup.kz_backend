<?php

namespace ns\geo;

use Yii;

class Geo extends SxGeo
{
	public function __construct($type = SXGEO_FILE){
		parent::__construct(__DIR__.DIRECTORY_SEPARATOR.'SxGeoCity.dat', $type);
	}
}
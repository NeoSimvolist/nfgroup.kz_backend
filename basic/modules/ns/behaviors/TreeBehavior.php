<?php

namespace ns\behaviors;

use Yii;
use paulzi\adjacencyList\AdjacencyListBehavior;

class TreeBehavior extends AdjacencyListBehavior
{
    public function beforeSave($insert)
    {
        if (!$this->operation && $this->owner->getIsNewRecord()) {
        	$this->operation = self::OPERATION_MAKE_ROOT;
        }
        if (parent::beforeSave($insert)) {
            return true;
        } else {
            return false;
        }
    }
}
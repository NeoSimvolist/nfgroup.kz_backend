<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\base\ErrorException;
use yii\helpers\Url;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use ns\firebase\Push;
use app\models\Webpush;
use app\models\TasksCategorys;
use app\models\Tasks;
use app\models\User;

class RabbitController extends Controller
{

    // send Dispatch
    public static function sendDispatch($data) {
        try {
            $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
            $channel = $connection->channel();
            
            $channel->queue_declare('dispatch', false, true, false, false);
            $msg = new AMQPMessage(json_encode($data), array('delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT));
            $channel->basic_publish($msg, '', 'dispatch');

            $channel->close();
            $connection->close();
            return true;
        } catch(ErrorException $e) {
            return false;
        }
    }

    // Push
    public static function push($recipients, $notification, $sendData) {
        $r = Push::send($recipients, $notification, $sendData);
        if (isset($r) && isset($r['results'])) {
            $hasError = false;
            foreach ($r['results'] as $key => $value) {
                if (isset($value['error'])) {
                    Webpush::deleteAll(['wbp_token' => $recipients[$key]]);
                    Yii::info($value['error'].': '.$recipients[$key], 'api');
                    $hasError = true;
                }
            }
            if ($hasError) Yii::getLogger()->flush(true);
        }
        return $r;
    }

    public static function pushWeb($recipients, $params) {
        $notification = array_merge($params['notification'], [
            'icon' => 'assets/icons/icon.png',
            'time_to_live' => 86400, // 24 часа,
            'click_action' => Url::base() . $params['notification']['click_action'],
        ]);
        return Self::push($recipients, $notification, $params['data']);
    }

    public static function pushAndroid($recipients, $params) {
        $data = array_merge([
            'iconColor' => '#2C3E50',
            'image' => 'www/assets/icons/icon.png',
            'image-type' => 'circle',
            'soundname' => 'default',
            'style' => 'inbox',
            'summaryText' => 'Новых сообщений: %n%',
            'ledColor' => [0, 0, 255, 0],
            'time_to_live' => 86400, // 24 часа
        ], $params['notification'], $params['data']);
        $data['redirect'] = $data['click_action'];
        unset($data['click_action']);
        return Self::push($recipients, null, $data);
    }

    // worker Dispatch
    public static function workerDispatch($data) 
    {
        try {
            if (!isset($data['scenario'])) 
                throw new ErrorException('Scenario not found');
            $scenario = $data['scenario'];

            $params = [
                'notification' => [],
                'data' => [
                    'scenario' => $scenario,
                ],
            ];

            switch ($scenario) {

                case 'winner':

                    if (!isset($data['task_id'])) {
                        throw new ErrorException('task_id not found');
                    }

                    if (!isset($data['user_id'])) {
                        throw new ErrorException('user_id not found');
                    }

                    // Текст сообщения
                    $task_id = $data['task_id'];
                    $user_id = $data['user_id'];

                    $params['notification']['title'] = 'Дело твоё';
                    $params['notification']['body'] = 'Дело твоё';
                    $params['notification']['click_action'] = '/othertasks/'.$task_id;

                    $params['data']['task_id'] = $task_id;

                    // Получатель - победитель дела
                    $webpush = (new \yii\db\Query())
                        ->from('webpush')
                        ->select(['webpush.wbp_token', 'webpush.wbp_platform'])
                        ->where(['webpush.wbp_user_id' => $user_id])
                        //->limit(1000)
                        ->all();
                    break;

                case 'newsolution':

                    if (!isset($data['task_id'])) {
                        throw new ErrorException('task_id not found');
                    }

                    if (!isset($data['user_id'])) {
                        throw new ErrorException('user_id not found');
                    }

                    // Текст сообщения
                    $task_id = $data['task_id'];
                    $user_id = $data['user_id'];
                    $message = isset($data['message']) ? $data['message'] : 'Тебе ответили';

                    $params['notification']['title'] = 'Тебе ответили';
                    $params['notification']['body'] = $message;
                    $params['notification']['click_action'] = '/mytasks/'.$task_id;

                    $params['data']['task_id'] = $task_id;

                    // Получатель - автор дела
                    $webpush = (new \yii\db\Query())
                        ->from('webpush')
                        ->select(['webpush.wbp_token', 'webpush.wbp_platform'])
                        ->where(['webpush.wbp_user_id' => $user_id])
                        //->limit(1000)
                        ->all();
                    break;

                case 'newuser':

                    if (!isset($data['user_id'])) {
                        throw new ErrorException('user_id not found');
                    }

                    // Текст сообщения
                    $user_id = $data['user_id'];

                    $user = User::findOne(['us_id' => $user_id]);
                    if (!isset($user)) {
                        throw new ErrorException('user not found');
                    }

                    $params['notification']['title'] = 'Новая регистрация';
                    $params['notification']['body'] = $user->us_name .' '.$user->us_phone;
                    $params['notification']['click_action'] = Url::base();

                    // Получатели только Админы - admin
                    $webpush = (new \yii\db\Query())
                        ->from('webpush')
                        ->select(['webpush.wbp_token', 'webpush.wbp_platform'])
                        ->join('JOIN', 'auth_assignment', 'auth_assignment.user_id = webpush.wbp_user_id')
                        ->where(['auth_assignment.item_name' => 'admin'])
                        //->limit(1000)
                        ->all();
                    break;

                case 'moderation':

                    if (!isset($data['task_id'])) {
                        throw new ErrorException('task_id not found');
                    }

                    // Текст сообщения
                    $task_id = $data['task_id'];
                    $message = isset($data['message']) ? $data['message'] : 'Новое дело на модерацию';

                    $params['notification']['title'] = 'Новое дело на модерацию';
                    $params['notification']['body'] = $message;
                    $params['notification']['click_action'] = '/othertasks/'.$task_id;

                    $params['data']['task_id'] = $task_id;

                    // Получатели только Модераторы - moderator
                    $webpush = (new \yii\db\Query())
                        ->from('webpush')
                        ->select(['webpush.wbp_token', 'webpush.wbp_platform'])
                        ->join('JOIN', 'auth_assignment', 'auth_assignment.user_id = webpush.wbp_user_id')
                        ->where(['auth_assignment.item_name' => ['moderator','admin']])
                        //->limit(1000)
                        ->all();
                    break;
                    
                case 'moderationSuccess':

                    if (!isset($data['task_id'])) {
                        throw new ErrorException('task_id not found');
                    }

                    if (!isset($data['author_id'])) {
                        throw new ErrorException('author_id not found');
                    }

                    // Текст сообщения
                    $task_id = $data['task_id'];
                    $author_id = $data['author_id'];

                    $params['notification']['title'] = 'Твое дело прошло модерацию';
                    $params['notification']['body'] = '/mytasks/'.$task_id;
                    $params['notification']['click_action'] = '/mytasks/'.$task_id;

                    $params['data']['task_id'] = $task_id;

                    // Получатель - Автор дела
                    $webpush = (new \yii\db\Query())
                        ->from('webpush')
                        ->select(['webpush.wbp_token', 'webpush.wbp_platform'])
                        ->where(['webpush.wbp_user_id' => $author_id])
                        //->limit(1000)
                        ->all();

                    break;
                case 'new': 
                    if (!isset($data['category_id'])) {
                        throw new ErrorException('category_id not found');
                    }
                    if (!isset($data['task_id'])) {
                        throw new ErrorException('task_id not found');
                    }
                    if (!isset($data['author_id'])) {
                        throw new ErrorException('author_id not found');
                    }

                    // Текст сообщения
                    $category_id = $data['category_id'];
                    $task_id = $data['task_id'];
                    $author_id = $data['author_id'];

                    $category = TasksCategorys::findOne(['tskct_id'=>$category_id ]);

                    $params['notification']['title'] = 'Новое дело';
                    $params['notification']['body'] = 'Тема: '.$category->tskct_name;
                    $params['notification']['click_action'] = '/othertasks/'.$task_id;

                    $params['data']['task_id'] = $task_id;
                    
                    // Получатели
                    $webpush = (new \yii\db\Query())
                        ->from('webpush')
                        ->select(['webpush.wbp_token', 'webpush.wbp_platform'])
                        ->join('LEFT JOIN', 'subscriptions', 'subscriptions.sbs_user_id = webpush.wbp_user_id')
                        ->where(['subscriptions.sbs_category_id' => $category_id])
                        ->andWhere(['<>', 'webpush.wbp_user_id', $author_id])
                        //->limit(1000)
                        ->all();

                    break;
                default:
                    throw new ErrorException('Unknown scenario');
                    break;
            }

            // Обрабатываем получателей
            $recipients = []; // web
            $recipientsAndroid = []; // android
            if (is_array($webpush) && !empty($webpush)) {
                echo 'Recipients: '.count($webpush).PHP_EOL;
                do {
                    $current = array_shift($webpush);
                    if (is_null($current)) break;
                    if ($current['wbp_platform'] == 1) {
                        $recipientsAndroid[] = $current['wbp_token'];
                    } else {
                        $recipients[] = $current['wbp_token'];
                    }
                    
                    // Web - Если уже накопилось 1000 получателей, отправляем
                    if (count($recipients) == 1000) {
                        echo 'Sending: '.count($recipients).PHP_EOL;
                        Self::pushWeb($recipients, $params);
                        $recipients = [];
                    } 
                    // Android - Если уже накопилось 1000 получателей, отправляем
                    if (count($recipientsAndroid) == 1000) {
                        echo 'Sending: '.count($recipientsAndroid).PHP_EOL;
                        Self::pushAndroid($recipientsAndroid, $params);
                        $recipientsAndroid = [];
                    } 
                } while (!is_null($current));
                // Web - Если остались не отправленные
                if (!empty($recipients)) {
                    echo 'Sending: '.count($recipients).PHP_EOL;
                    Self::pushWeb($recipients, $params);
                    $recipients = [];
                }
                // Android - Если остались не отправленные
                if (!empty($recipientsAndroid)) {
                    echo 'Sending: '.count($recipientsAndroid).PHP_EOL;
                    Self::pushAndroid($recipientsAndroid, $params);
                    $recipientsAndroid = [];
                } 
            } else {
                throw new ErrorException('Recipients not found');
            }
        } catch(ErrorException $e) {
            echo 'Error: '.$e->getMessage().PHP_EOL; 
        }

    }

    public function actionSend()
    {
        Self::sendDispatch([
            'category_id' => 10,
            'task_id' => 26,
        ]);
    }

    // Worker
    public function actionRecive()
    {

        $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
        $channel = $connection->channel();
        $channel->queue_declare('dispatch', false, true, false, false); 

        echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";
        $callback = function($msg) {
            echo " [x] Received ", $msg->body, "\n";
            try {
                Self::workerDispatch(json_decode($msg->body, true));
            } catch(ErrorException $e) {}
            echo " [x] Done", "\n"; 
            // Сообщение доставлено
            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
        };
        //$channel->basic_qos(null, 1, null); // Значит сервис не получет больше 1-го сообщения за раз
        $channel->basic_consume('dispatch', '', false, false, false, false, $callback);
        while(count($channel->callbacks)) {
            $channel->wait();
        }

        $channel->close();
        $connection->close();
        /**/
    }

    public static function websocket($data = null) {
    	$queueName = 'websocket';
    	$data = isset($data) ? $data : 'Тестовое сообщение';
      try {
          $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
          $channel = $connection->channel();
          $channel->queue_declare($queueName, false, false, false, false);
          $msg = new AMQPMessage(json_encode($data, JSON_UNESCAPED_UNICODE));
          $channel->basic_publish($msg, '', $queueName);
          $channel->close();
          $connection->close();
          return true;
      } catch(ErrorException $e) {
          return false;
      }
    }

    public function actionWebsocket() {
    	Self::websocket([
    		'event' => 'websocket',
    		'model' => 'reviews',
    		'action' => 'create',
    	]);
    }

}
<?php

namespace ns\rest;

use yii;
use yii\rest\ActiveController;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\Cors;
use yii\filters\VerbFilter;
use yii\filters\ContentNegotiator;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

class RestFullController extends ActiveController
{

    public $authorization = true;
    public $rbac = true;

    /*
    public function init() {
        parent::init();
        $handler = new \ns\rest\ApiErrorHandler;
        \Yii::$app->set('errorHandler', $handler);
        $handler->register();
    }
    */

    public function prepareRules() {
        return [
            'options' => [
              'roles' => ['guest'],
            ],
            'index' => [
              'roles' => ['user'],
            ],
            'view' => [
              'roles' => ['user'],
            ],
            'create' => [
              'roles' => ['create'],
            ],
            'delete' => [
              'roles' => ['delete'],
            ],
            'update' => [
              'roles' => ['update'],
            ],
        ];
    }

    public function behaviors()
    {

        $behaviors = parent::behaviors();
        
        if ($this->authorization === true) {
            $behaviors['authenticator'] = [
                'class' => CompositeAuth::className(),
                'authMethods' => [
                    HttpBearerAuth::className(),
                    QueryParamAuth::className(),
                ],
                // Для Action-а options не нужна авторизация
                'except' => ['options'],
            ];
            // Если включен rbac, авторизацию делаем опциональной, и контроль возлагается на rbac
            if ($this->rbac === true) { 
            	$behaviors['authenticator']['optional'] = ['*'];
            }
        }

        if ($this->rbac === true) {
            $behaviors['access'] = [
                'class' => \yii\filters\AccessControl::className(),
                //'only' => array_keys($this->prepareRules()),
                'rules' => [],
		           	'denyCallback' => function($rule, $action){
		           		throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
		           	}
            ];
            foreach ($this->prepareRules() as $action => $params) {
                $behaviors['access']['rules'][] = ArrayHelper::merge([
                    'allow' => true,  
                    'actions' => [$action],  
                ], $params);
            }
        }

        return ArrayHelper::merge($behaviors, [
            'corsFilter' => [
                'class' => Cors::className(),
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST', 'PUT', 'PATCH', 'GET', 'OPTIONS', 'DELETE'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Allow-Origin' => ['*'],
                    'Access-Control-Allow-Headers' => ['*'],
                    'Access-Control-Expose-Headers' => [
                        'X-Pagination-Total-Count',
                        'X-Pagination-Page-Count',
                        'X-Pagination-Current-Page',
                        'X-Pagination-Per-Page',
                        'Link',
                    ],
                ],
            ],
        ]);
        
    } 

}
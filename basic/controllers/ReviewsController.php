<?php

namespace app\controllers;

use Yii;
use ns\rest\RestFullController;

class ReviewsController extends RestFullController
{
  public $modelClass = 'app\models\Reviews';
  public function prepareRules() {
      $rules = parent::prepareRules();
      $rules['index'] = [
        'allow' => true,
        'roles' => ['guest'],
      ];
      $rules['view'] = [
        'allow' => true,
        'roles' => ['guest'],
      ];
      $rules['create'] = [
        'allow' => true,
        'roles' => ['guest'],
      ];
      return $rules;
  }
}
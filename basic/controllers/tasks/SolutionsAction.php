<?php

namespace app\controllers\tasks;

use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\rest\Action;

class SolutionsAction extends Action
{
    public function run($id)
    {
        $model = $this->findModel($id);
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }
        $query = $model->getSolutions();
        if (!(Yii::$app->user->can('moderation') || Yii::$app->user->can('isAuthor', ['id' => $model->tsk_author_id]))) {
            $query = $query->andWhere(['sl_author_id' => Yii::$app->user->identity->us_id]);
        }
        return Yii::createObject([
            'class' => ActiveDataProvider::className(),
            'query' => $query,
        ]);
    }

}
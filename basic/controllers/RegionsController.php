<?php

namespace app\controllers;

use Yii;
use ns\rest\RestFullController;

class RegionsController extends RestFullController
{
    public $modelClass = 'app\models\Regions';
}
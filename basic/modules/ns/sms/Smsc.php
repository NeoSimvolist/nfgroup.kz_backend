<?php

namespace ns\sms;

class Smsc
{
	static function send($phones, $message, $query = []) {
		return send_sms($phones, $message, $query);
	}
}

define("SMSC_URL", "https://smsc.ru/sys/send.php");
define("SMSC_LOGIN", "NeoSimvolist");
define("SMSC_PSW", "5dc420517257171fef492d9985283a6e");

function send_sms($phones, $msg, $query = []) {
	$url = 'https://smsc.ru/sys/send.php';

	$data = array(
		'login' => SMSC_LOGIN, 
		'psw' => SMSC_PSW,
		'phones' => preparePhones($phones),
		'mes' => $msg,
		'fmt' => 3,
		'cost' => 3,
		'charset' => 'utf-8',
	);
	$data = array_merge($data, $query);

	// use key 'http' even if you send the request to https://...
	$options = array(
	    'http' => array(
	        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
	        'method'  => 'POST',
	        'content' => http_build_query($data)
	    )
	);
	$context  = stream_context_create($options);
	$result = file_get_contents(SMSC_URL, false, $context);
	if ($result === FALSE) { 
		return [
			'error' => 'Сервис не отвечает',
			'error_code' => 500,
		];
	}
	return json_decode($result, true);
}

function preparePhones($phones) {
	$a = explode(',', $phones);
	$aRslt = [];
	foreach ($a as $key => $phone) {
		$phone = preg_replace('/[^0-9,]/','',$phone);
		$cnt = strlen($phone);
		if ($cnt == 11) {
			$firstLetter = substr($phone, 0, 1);
			if (in_array($firstLetter, ['8'])) 
				$phone = substr_replace($phone, '7', 0, 1);
		} elseif ($cnt == 10) {
			$firstLetter = substr($phone, 0, 1);
			if (in_array($firstLetter, ['7'])) {
				$phone = substr_replace($phone, '7', 0, 0);	
			} else {
				continue;
			}
		} else {
			continue;
		}
		$phone = substr_replace($phone, '+', 0, 0);
		$aRslt[] = $phone;
	}
	return implode(',', $aRslt);
};
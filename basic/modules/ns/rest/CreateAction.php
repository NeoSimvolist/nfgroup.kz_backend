<?php

namespace ns\rest;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use yii\web\ServerErrorHttpException;
use yii\rest\Action;

class CreateAction extends Action
{

    public $scenario = Model::SCENARIO_DEFAULT;
    public $viewAction = 'view';

    public function run($id)
    {
        $modelRoot = $this->findModel($id);

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $model = new $this->modelClass([
            'scenario' => $this->scenario,
        ]);

        $model->load(Yii::$app->getRequest()->getBodyParams(), '');

        $result = false;
        switch ($this->id) {
            case 'prepend':
                $result = $model->prependTo($modelRoot)->save();
                break;
            case 'append':
                $result = $model->appendTo($modelRoot)->save();
                break;
            case 'before':
                $result = $model->insertBefore($modelRoot)->save();
                break;
            case 'after':
                $result = $model->insertAfter($modelRoot)->save();
                break;
        }

        if ($result) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $id = implode(',', array_values($model->getPrimaryKey(true)));
            $response->getHeaders()->set('Location', Url::toRoute([$this->viewAction, 'id' => $id], true));
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        return $model;
    }
}

<?php

namespace ns\rest;

use yii;
use yii\rest\Controller;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\Cors;
use yii\filters\VerbFilter;
use yii\filters\ContentNegotiator;
use yii\helpers\ArrayHelper;
use yii\web\Response;

class RestController extends Controller
{

    public $authorization = true;

    public function init() {
        parent::init();

        /*
            Код гарантирует что ответ будет в томже формате что и запрос, н-р: xml (по умолчанию)
        */
        $o = new yii\filters\ContentNegotiator();
        $o->formats = [
            'application/json' => Response::FORMAT_JSON,
            'application/xml' => Response::FORMAT_XML,
            'text/html' => YII_DEBUG ? Response::FORMAT_XML : Response::FORMAT_JSON,
        ];
        $o->negotiate();

        // logging
        //Yii::info(Yii::$app->request->rawBody, 'api');
    }
    
    public function actions()
    {
        return [
            'options' => [
                'class' => 'yii\rest\OptionsAction',
            ],
        ];
    }

    public function behaviors()
    {

        $behaviors = parent::behaviors();
        
        if ($this->authorization === true) {
            $behaviors['authenticator'] = [
                'class' => CompositeAuth::className(),
                'authMethods' => [
                    HttpBearerAuth::className(),
                    QueryParamAuth::className(),
                ],
                // Для Action-а options не нужна авторизация
                'except' => ['options'],
            ];
        }

        return ArrayHelper::merge($behaviors, [
            'corsFilter' => [
                'class' => Cors::className(),
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST', 'PUT', 'PATCH', 'GET', 'OPTIONS', 'DELETE'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Allow-Origin' => ['*'],
                    'Access-Control-Allow-Headers' => ['*'],
                    'Access-Control-Expose-Headers' => [
                        'X-Pagination-Total-Count',
                        'X-Pagination-Page-Count',
                        'X-Pagination-Current-Page',
                        'X-Pagination-Per-Page',
                        'Link',
                    ],
                ],
            ],
        ]);
        
    } 

}
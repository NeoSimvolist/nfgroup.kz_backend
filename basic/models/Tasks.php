<?php

namespace app\models;

use Yii;
use app\models\User;
use app\commands\RabbitController;
use yii\db\ActiveRecord;
/**
This is the model class for table "tasks".
 *
@property integer $tsk_id
@property string $tsk_text
@property string $tsk_created
@property integer $tsk_author_id
@property integer $tsk_status_id
@property string $tsk_files
 */
class Tasks extends \yii\db\ActiveRecord
{

    const SCENARIO_BEST = 'best';

    /**
    @inheritdoc
     */
    public static function tableName()
    {
        return 'tasks';
    }

    public static function find()
    {   
        $view = Yii::$app->request->get('view');
        $query = parent::find();
        $query->andWhere(['tsk_delete' => 0]);
        switch ($view) {
            // Собственные заявки пользователя
            case 'my':
                return $query->andWhere(['tsk_author_id'=>Yii::$app->user->identity->us_id]);
                break;

            // Заявки на которые пользователь подписан в статусе 2 - Поиск исполнителя...
            case 'subscriptions':
                $r = [];
                $tmp = Yii::$app->user->identity->getSubscriptions()->select('sbs_category_id')->all();
                foreach ($tmp as $key => $value) {
                    $r[] = $value['sbs_category_id'];
                }
                return $query->andWhere(['tsk_category_id'=>$r, 'tsk_status_id' => 2]);

            // Решенные дела в которых пользователь победитель
            case 'winner':
                return $query->andWhere(['tsk_winner_id'=>Yii::$app->user->identity->us_id, 'tsk_status_id' => 3]);

            default:
                if (Yii::$app->user->can('reader')) {
                    return $query;
                } elseif (Yii::$app->user->can('moderation')) {
                    return $query->andWhere(['tsk_status_id' => [1,2]]);    
                } else {
                    throw new \yii\web\ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));   
                }
                break;
        }
    }

    public function behaviors()
    {
        return [
            // Автор и редактор записи
            [
                'class' => \yii\behaviors\BlameableBehavior::className(),
                'createdByAttribute' => 'tsk_author_id',
                'updatedByAttribute' => 'tsk_editor_id',
            ],
            // Дата создания и последнего редактирования записи
            [
                'class' => \ns\behaviors\DatetimeBehavior::className(),
                'createdAtAttribute' => 'tsk_created',
                'updatedAtAttribute' => 'tsk_updated',
            ],
            // JsonFieldBehavior
            [
                'class' => \skobka\jsonField\behaviors\JsonFieldBehavior::className(),
                'dataField' => 'tsk_files',
            ],
            // Ловим событие update модели
            [
                'class' => \yii\behaviors\AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'tsk_status_id',
                ],
                'value' => function ($event) {
                    // Если атрибут был изменен и стал равен 2 - Модерация одобрена
                    if ($this->isAttributeChanged('tsk_status_id') && ($this->tsk_status_id == 2)) {
                        // Уведомляем всех о новом Деле
                        RabbitController::sendDispatch([
                            'scenario' => 'new',
                            'category_id' => $this->tsk_category_id,
                            'task_id' => $this->tsk_id,
                            'message' => $this->tsk_text, // !текст
                            'author_id' => $this->tsk_author_id,
                        ]);
                        // Уведомление автору об успешном прохождении модерации
                        RabbitController::sendDispatch([
                            'scenario' => 'moderationSuccess',
                            'task_id' => $this->tsk_id,
                            'author_id' => $this->tsk_author_id,
                        ]);
                    }
                    return $this->tsk_status_id;
                },
            ],
        ];
    }

    /**
    @inheritdoc
     */
    public function rules()
    {
        $rules = [
        // tsk_files
            [['tsk_text'], 'string', 'min' => 10, 'max' => 300],
            [['tsk_text'], 'required'],
            // tsk_files
            ['tsk_files', 'safe'],
            ['tsk_category_id', 'integer'],
            ['tsk_city_id', 'integer'],
            // tsk_status_id
            ['tsk_status_id', 'integer'],
            ['tsk_status_id', 'default', 'value' => 1],
        ];
        // Если модератор, делаем поле tsk_category_id обязательным
        if (Yii::$app->user->can('moderation')) {
            $rules[] = ['tsk_category_id', 'required'];    
        }
        return $rules;
    }

    /**
    @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tsk_id' => Yii::t('app', 'Tsk ID'),
            'tsk_text' => Yii::t('app', 'Tsk Text'),
            'tsk_created' => Yii::t('app', 'Tsk Created'),
            'tsk_author_id' => Yii::t('app', 'Tsk Author ID'),
            'tsk_status_id' => Yii::t('app', 'Tsk Status ID'),
            'tsk_files' => Yii::t('app', 'Tsk Files'),
        ];
    }

    public function fields()
    {
        $fields = parent::fields();

        // Добавляем поля в выдачу
        $fields['tsk_author_name'] = function() {
            if (!isset($this->author)) return null;
            return $this->author->us_name;
        };
        $fields['tsk_status_name'] = function() {
            if (!isset($this->status)) return null;
            return $this->status->tskst_name;
        };
        $fields['tsk_category_name'] = function() {
            if (!isset($this->category)) return null;
            return $this->category->tskct_name;
        };
        $fields['tsk_city_name'] = function() {
            if (!isset($this->city)) return null;
            return $this->city->name_ru;
        };
        $fields['tsk_city'] = function() {
            return $this->city;
        };
        $fields['tsk_solutions_count'] = function() {
            return (int) $this->getSolutions()->count();
        };
        $fields['tsk_winner_id'] = function() {
            return $this->tsk_winner_id;
        };

        // Если текущий пользователь есть победитель, то показываем ему контакты
        if ($this->tsk_winner_id == Yii::$app->user->identity->us_id) {
            $fields['tsk_author'] = function() {
                return $this->getAuthor()->one();
            };
        }
        return $fields;
    }

    /* 
        Связь с Users / Автор
    */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['us_id' => 'tsk_author_id']);
    }

    /* 
        Связь с TasksStatus / Статус
    */
    public function getStatus()
    {
        return $this->hasOne(TasksStatus::className(), ['tskst_id' => 'tsk_status_id']);
    }

    /* 
        Связь с TasksCategorys / Категории
    */
    public function getCategory()
    {
        return $this->hasOne(TasksCategorys::className(), ['tskct_id' => 'tsk_category_id']);
    }

    /* 
        Связь с Cities / Города
    */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'tsk_city_id']);
    }

    /* 
        Связь с Solutions / Решения
    */
    public function getSolutions()
    {
        return $this->hasMany(Solutions::className(), ['sl_task_id' => 'tsk_id']);
    }


    public function afterSave( $insert, $changedAttributes ) {
        parent::afterSave($insert, $changedAttributes);
        if ($insert == true) {
            // На модерацию
            RabbitController::sendDispatch([
                'scenario' => 'moderation',
                'task_id' => $this->tsk_id,
                'message' => $this->tsk_text, // !текст
            ]);
        }
    }
}

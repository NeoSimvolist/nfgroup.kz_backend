<?php

namespace app\commands;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{

    public function actionRemoveall()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();
    }

    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();

        // Правила
        $isAuthorRule = new \app\rbac\AuthorRule;
        $auth->add($isAuthorRule);

        $isAuthor = $auth->createPermission('isAuthor');
        $isAuthor->description = 'Создатель записи';
        $isAuthor->ruleName = $isAuthorRule->name;
        $auth->add($isAuthor);

        // Разрешения
        $reader = $auth->createPermission('reader');
        $reader->description = 'Видит все';
        $auth->add($reader);

        $moderation = $auth->createPermission('moderation');
        $reader->description = 'Разрешения модератора';
        $auth->add($moderation);

        $create = $auth->createPermission('create');
        $create->description = 'Разрешено создание';
        $auth->add($create);

        $delete = $auth->createPermission('delete');
        $delete->description = 'Разрешено удаление';
        $auth->add($delete);

        $update = $auth->createPermission('update');
        $update->description = 'Разрешено редактирование';
        $auth->add($update);

        // Роль guest
        $guest = $auth->createRole('guest'); 
        $auth->add($guest);

        // Роль user
        $user = $auth->createRole('user'); 
        $auth->add($user);
        $auth->addChild($user, $guest); // user может тоже что и guest
        $auth->addChild($user, $create); // user может создавать
        $auth->addChild($user, $isAuthor); // Является создателем записи

        // Роль admin
        $admin = $auth->createRole('admin'); 
        $auth->add($admin);
        $auth->addChild($admin, $user); // Админ может тоже что и user
        $auth->addChild($admin, $reader); // Видит все
        $auth->addChild($admin, $delete); // Админ может удалять
        $auth->addChild($admin, $update); // Админ может редактировать

        $auth->assign($admin, 6);
    }

    public function actionAssign()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');
        $auth->assign($admin, 6);
    }
}
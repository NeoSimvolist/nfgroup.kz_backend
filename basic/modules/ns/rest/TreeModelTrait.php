<?php

namespace ns\rest;

use Yii;

trait TreeModelTrait
{

    public function fields()
    {
        $fields = parent::fields();
        $childs = Yii::$app->request->get('childs');
        $childs = filter_var($childs, FILTER_VALIDATE_BOOLEAN);
        if ($childs === true) {
            $fields['childs'] = function() {
                return $this->children;
            };
            $fields['childs_count'] = function() {
                return count($this->children);
            };
        } else {
            $fields['childs_count'] = function() {
                return count($this->children);
            };
        }
        return $fields;
    }
    
}
<?php

namespace ns\models;

use Yii;

/**
 * ScopeQuery - для разделения запросов на Scope-ы/области видимости по принципу организаций 
 */
class ScopeQuery extends \yii\db\ActiveQuery
{

    /**
     */
    public function all($db = null)
    {
        $this->andWhere('scope_id = :scope_id',[
            ':scope_id' => 0,   
        ]);
        return parent::all($db);
    }

    /**
     */
    public function one($db = null)
    {
        $this->andWhere('scope_id = :scope_id',[
            ':scope_id' => 0,   
        ]);
        return parent::one($db);
    }
}

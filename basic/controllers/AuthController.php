<?php

namespace app\controllers;

use Yii;
use yii\rest\Controller;
use yii\rest\ActiveController;
use yii\rest\Serializer;
use yii\helpers\ArrayHelper;
use yii\filters\ContentNegotiator;
use yii\filters\Cors;
use yii\web\Response;
use yii\web\ServerErrorHttpException;
use app\models\LoginForm;
use app\models\User;
use app\models\Webpush;
use ns\models\Scenarios;
use ns\rest\RestController;
use ns\sms\Smsc;

class AuthController extends RestController
{
    public $authorization = false;

	public function actionLogin()
	{
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post(), '') && $model->login()) {

            /* 
                WEB PUSH 
                Актаулизуем токен при каждом входе, если передан параметр tokenpush
            */
            $tokenpush = Yii::$app->request->post('tokenpush');
            // Обновляем токен
            if (!empty($tokenpush)) {
                $push = Webpush::findOne(['wbp_token' => $tokenpush, 'wbp_user_id' => Yii::$app->user->identity->us_id]);
                if (isset($push)) {
                    // Актуализуем дату
                    $push->wbp_updated = (new \DateTime)->format('Y-m-d H:i:s');
                } else {
                    $push = new Webpush();    
                    $push->wbp_token = $tokenpush;
                }
                $push->save();
            }

            return [
                'access_token' => Yii::$app->user->identity->getAuthKey(),
                'user' => User::findIdentityByAccessToken(Yii::$app->user->identity->getAuthKey()),
            ];
	    } else {
	        $model->validate();
	        return $model;
	    }
	}

    public function actionSignup()
    {
        $model = new User(['scenario'=>User::SCENARIO_SIGNUP]);
        $model->load(Yii::$app->request->post(), '');
        if ($model->validate()) {
            $pin = str_pad(mt_rand(1, 999999),6,mt_rand(1, 9),STR_PAD_LEFT);
            $scenario = new Scenarios();
            $aTmp = $model->toArray();
            $aTmp['pin'] = hash('md5', $pin);
            $aTmp['pin2'] = $pin; // Дублируем для тестирования
            $aTmp['us_pass'] = $aTmp['pin'];
            $scenario->setData($aTmp);
            //ArrayHelper::filter($this->toArray(), ['sc_token']));
            if ($scenario->validate() && $scenario->save()) {}
            
            /* 
                Здесь отправляем код верификации/пин по надежному источнику
            */
            $r = Smsc::send($model->us_phone, 'Твоя активация и пароль: '.$pin.'. poreshay.com');
            if (isset($r['error'])) {
                $model->addError('us_phone', 'Блин.. SMS не отправляется. Попробуй позже');
                return $model;
            }
            /*
            // Отправляем на email
            Yii::$app->mailer->compose()
                ->setFrom('admin@leads.nfgroup.kz')
                ->setTo($model->us_email)
                ->setSubject('Вы зарегистрировались на сайте')
                ->setTextBody('Ваш пин код для активации: '.$pin)
                ->setHtmlBody('<b>Ваш пин код для активации: '.$pin.'</b>')
                ->send();
            */
            return $scenario->toRender();
        }
        return $model;
    }

    public function actionSignupconfirm()
    {
        $model = new User(['scenario'=>User::SCENARIO_CONFIRM]);
        $model->load(Yii::$app->request->post(), '');
        if ($model->validate()) {
            $scenario = Scenarios::findByToken($model->scenario_token);
            if (isset($scenario)) {
                // Проверяем pin 
                $attribute = 'pin';
                if (hash('md5', $model->$attribute) !== $scenario->getData()[$attribute]) {
                    $model->addError($attribute, Yii::t('app', 'invalid_pin')); 
                    return $model;
                }

                // Подгружаем данные о пользователе из сценария
                $user = new User(['scenario' => User::SCENARIO_PROFILE]);
                $user->load($scenario->getData(), '');
                if ($user->validate()) {
                    $user->save();
                    // Удаляем сценарий в случае успеха. В случае неуспеха сценарий будет жить столько сколько указано в sc_lifetime
                    $scenario->delete();
                }
                return [
                    'access_token' => $user->getAuthKey(),
                    'user' => $user,
                ];
            } else {
                $model->addError('scenario_token', Yii::t('app', 'script_notfound'));
                return $model;
            }
        }
        return $model;
    }

    public function actionTest()
    {
        return str_pad(mt_rand(1, 999999),6,mt_rand(1, 9),STR_PAD_LEFT);
    }

}

<?php

namespace ns\models;

use Yii;
use yii\helpers\Json;
use yii\rest\Serializer;
use yii\helpers\ArrayHelper;
use ns\validators\JsonValidator;
use ns\NsHelper;
/**
 * This is the model class for table "scenarios".
 *
 * @property integer $sc_id
 * @property string $sc_token
 * @property string $sc_data_json
 * @property string $sc_data_hash
 * @property integer $sc_type
 * @property integer $sc_step
 * @property integer $sc_ipv4
 * @property string $sc_created
 * @property integer $sc_lifetime
 */
class Scenarios extends \yii\db\ActiveRecord
{

    const SCENARIO_NEXT = 'next';
    const SCENARIO_CONFIRM = 'confirm';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scenarios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            /* All scenario */
            // sc_data_json
            ['sc_data_json', 'string'],
            ['sc_data_json', JsonValidator::className()],
            ['sc_data_json', 'required'],

            // sc_type
            ['sc_type', 'integer'],
            ['sc_type', 'default', 'value' => 0],
            ['sc_type', 'required'],
            
            // sc_ipv4
            ['sc_ipv4', 'integer'],
            ['sc_ipv4', 'default', 'value' => function($model, $attribute) {
                return ip2long($_SERVER['REMOTE_ADDR']);  
            }],
            // sc_lifetime
            ['sc_lifetime', 'integer'],
            ['sc_lifetime', 'default', 'value' => 1440],
            // sc_created
            ['sc_created', 'default', 'value' => (new \DateTime)->format('Y-m-d H:i:s')],
            ['sc_created', 'date', 'format' => 'php:Y-m-d H:i:s'],
            // sc_token
            ['sc_token', 'string', 'max' => 32],
            ['sc_token', 'default', 'value' => function($model, $attribute) {
                return hash('md5',  $model->sc_created . $model->sc_ipv4 . $model->sc_type . $model->sc_data_json); 
            }],

            /* SCENARIO_NEXT */
            // sc_token
            ['sc_token', 'required', 'on' => self::SCENARIO_NEXT],
        ];
    }

    public static function findByToken($token) 
    {
        return Self::findOne(['sc_token' => $token]);
    }

    public function setData($data) 
    {
        $this->sc_data_json = Json::encode($data);
    }

    public function getData() 
    {
        return NsHelper::json_decode($this->sc_data_json, []);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sc_id' => Yii::t('app', 'Sc ID'),
            'sc_token' => Yii::t('app', 'Sc Token'),
            'sc_data_json' => Yii::t('app', 'Sc Data Json'),
            'sc_type' => Yii::t('app', 'Sc Type'),
            'sc_ipv4' => Yii::t('app', 'Sc Ipv4'),
            'sc_created' => Yii::t('app', 'Sc Created'),
            'sc_lifetime' => Yii::t('app', 'Sc Lifetime'),
        ];
    }

    public function toRender()
    {
        return ($this->hasErrors()) ? $this : (new Serializer())->serialize(ArrayHelper::filter($this->toArray(), ['sc_token']));
    }

    /*

    public function beforeValidate()
    {
        return parent::beforeValidate();
    }

    public function beforeSave($insert)
    {
        return parent::beforeSave($insert);
    }

    public function init()
    {
        parent::init();
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_NEXT] = ['sc_token', 'sc_data_json', 'sc_data_hash'];
        return $scenarios;
    }
    */
}

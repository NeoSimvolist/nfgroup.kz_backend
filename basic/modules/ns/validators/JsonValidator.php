<?php

namespace ns\validators;

use Yii;
use yii\validators\Validator;
use ns\NsHelper;

class JsonValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        if (!NsHelper::is_json($model->$attribute)) {
            $model->addError($attribute, Yii::t('app', 'invalid_json', ['attribute'=>'']));  
        }
    }
}

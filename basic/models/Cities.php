<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sxgeo_cities".
 *
 * @property string $id
 * @property string $region_id
 * @property string $name_ru
 * @property string $name_en
 * @property string $lat
 * @property string $lon
 * @property string $okato
 */
class Cities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sxgeo_cities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'region_id', 'name_ru', 'name_en', 'lat', 'lon', 'okato'], 'required'],
            [['id', 'region_id'], 'integer'],
            [['lat', 'lon'], 'number'],
            [['name_ru', 'name_en'], 'string', 'max' => 128],
            [['okato'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'region_id' => Yii::t('app', 'Region ID'),
            'name_ru' => Yii::t('app', 'Name Ru'),
            'name_en' => Yii::t('app', 'Name En'),
            'lat' => Yii::t('app', 'Lat'),
            'lon' => Yii::t('app', 'Lon'),
            'okato' => Yii::t('app', 'Okato'),
        ];
    }

    public function fields()
    {
        $fields = parent::fields();

        // Добавляем поля в выдачу
        $fields['region_name'] = function() {
            if (!isset($this->region)) return null;
            return $this->region->name_ru;
        };

        $fields['country_name'] = function() {
            if (!isset($this->region)) return null;
            if (!isset($this->region->countrys)) return null;
            return $this->region->countrys->name_ru;
        };

        return $fields;
    }

    public static function find()
    {   
        return parent::find()->where(['remove' => 0])->orderBy(['name_ru'=>SORT_ASC]);
    }

    /* 
        Связь с Regions / Регионы
    */
    public function getRegion()
    {
        return $this->hasOne(Regions::className(), ['id' => 'region_id']);
    }
}

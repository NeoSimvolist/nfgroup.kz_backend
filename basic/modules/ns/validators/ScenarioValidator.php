<?php

namespace ns\validators;

use Yii;
use yii\validators\Validator;
use ns\NsHelper;
use ns\models\Scenarios;

class ScenarioValidator extends Validator
{
	public $equalto = null;

    public function validateAttribute($model, $attribute)
    {	
    	if (!empty($this->equalto)) {
    		$field_name = $this->equalto;

    		$scenario = Scenarios::findByToken($model->$attribute);
    		if (!$scenario) {
    			$model->addError($attribute, Yii::t('app', 'Сценарий не найден'));
    			return;
    		}
	    	if ($model->$field_name !== $scenario->getData()->$field_name)
	        	$model->addError($attribute, Yii::t('app', 'Неверный пинкод')); 
    	}
    }
}

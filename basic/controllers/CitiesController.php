<?php

namespace app\controllers;

use Yii;
use ns\rest\RestFullController;
use yii\data\ActiveDataProvider;

class CitiesController extends RestFullController
{
    public $modelClass = 'app\models\Cities';
    public $authorization = false;

    public function actionFind()
    {
    	$class = $this->modelClass;
    	$search = Yii::$app->request->get('search');
    	$query = (empty($search)) ? $class::find() : $class::find()->andWhere(['like', 'name_ru', $search]);
			return new ActiveDataProvider([
			    'query' => $query,
			]);
    }

    public function prepareRules() {
        $rules = parent::prepareRules();
        $rules['find'] = [];
        return $rules;
    }

}
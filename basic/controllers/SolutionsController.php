<?php

namespace app\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use ns\rest\RestFullController;

/* 
	Все Заявки
*/
class SolutionsController extends RestFullController
{
    public $modelClass = 'app\models\Solutions';

    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            // solutions nodes
            'scenario' => [
                'class' => 'app\controllers\solutions\ScenarioAction',
                'modelClass' => $this->modelClass,
            ],
        ]);
    }

    public function prepareRules() {
        $rules = parent::prepareRules();
        $rules['scenario'] = ['roles' => ['author']];
        return $rules;
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        switch ($action) {
            case 'scenario':
                if (!(
                    (Yii::$app->user->can('isAuthor', ['id' => $model->task->tsk_author_id]) && $model->task->tsk_status_id == 2)
                )) {
                    throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
                }
                break;
            
            default:
                # code...
                break;
        }
    }

}
<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sxgeo_regions".
 *
 * @property string $id
 * @property string $iso
 * @property string $country
 * @property string $name_ru
 * @property string $name_en
 * @property string $timezone
 * @property string $okato
 */
class Regions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sxgeo_regions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'iso', 'country', 'name_ru', 'name_en', 'timezone', 'okato'], 'required'],
            [['id'], 'integer'],
            [['iso'], 'string', 'max' => 7],
            [['country'], 'string', 'max' => 2],
            [['name_ru', 'name_en'], 'string', 'max' => 128],
            [['timezone'], 'string', 'max' => 30],
            [['okato'], 'string', 'max' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'iso' => Yii::t('app', 'Iso'),
            'country' => Yii::t('app', 'Country'),
            'name_ru' => Yii::t('app', 'Name Ru'),
            'name_en' => Yii::t('app', 'Name En'),
            'timezone' => Yii::t('app', 'Timezone'),
            'okato' => Yii::t('app', 'Okato'),
        ];
    }

    public function fields()
    {
        $fields = parent::fields();

        // Добавляем поля в выдачу
        $fields['country_name'] = function() {
            if (!isset($this->countrys)) return null;
            return $this->countrys->name_ru;
        };

        return $fields;
    }

    /* 
        Связь с Country / Страны
    */
    public function getCountrys()
    {
        return $this->hasOne(Country::className(), ['iso' => 'country']);
    }
}

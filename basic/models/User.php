<?php

namespace app\models;

use Yii;
use \yii\helpers\ArrayHelper;
use app\commands\RabbitController;

class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{

    const SCENARIO_SIGNUP = 'signup';
    const SCENARIO_CONFIRM = 'confirm';
    const SCENARIO_PROFILE = 'profile';

    public $scenario_token;
    public $pin;

    /**/
    public static function tableName()
    {
        return 'users';
    }
    
    public function fields()
    {
        $fields = parent::fields();

        if (!in_array($this->scenario, [self::SCENARIO_SIGNUP, self::SCENARIO_CONFIRM])) {
            unset($fields['us_id']);
            unset($fields['us_pass']);
            unset($fields['us_access_token']);
        }
        /*
        // Добавляем поля в выдачу
        $fields['pin'] = function() {
            return $this->pin;
        };
        $fields['scenario_token'] = function() {
            return $this->scenario_token;
        };
        */
        $fields['rbac'] = function() {
            return [
                'roles' => array_keys(ArrayHelper::getColumn(Yii::$app->authManager->getRolesByUser($this->us_id), 'name')),
                'permissions' => array_keys(ArrayHelper::getColumn(Yii::$app->authManager->getPermissionsByUser($this->us_id), 'name')),
            ];
        };

        return $fields;
    }

    public function rules()
    {
        return [

            // us_name
            ['us_name', 'string', 'min' => 2, 'max' => 20],
            // us_email
            ['us_email', 'unique', 'message' => Yii::t('app', 'unique_email')],
            ['us_email', 'email'],
            // us_phone
            ['us_phone', 'string', 'min'=>11, 'max'=>11],
            ['us_phone', 'unique', 'message' => Yii::t('app', 'unique_phone')],
            // us_city_id
            ['us_city_id', 'integer'],
            // us_access_token
            ['us_access_token', 'string'],
            ['us_access_token', 'default', 'value' => function($model, $attribute){
                return hash('md5', $model->us_name . $model->us_email . $model->us_phone . (new \DateTime)->format('Y-m-d H:i:s') );    
            }],
            // role
            //['role', 'string'],
            /* SCENARIO_PROFILE */
            // us_pass
            ['us_pass', 'string', 'on' => self::SCENARIO_PROFILE],

            /* SCENARIO_SIGNUP */
            // us_name
            ['us_name', 'required', 'on' => self::SCENARIO_SIGNUP],
            // us_phone
            ['us_phone', 'required', 'on' => self::SCENARIO_SIGNUP],
            // us_city_id
            ['us_city_id', 'required', 'on' => self::SCENARIO_SIGNUP],
            /* SCENARIO_CONFIRM */
            ['scenario_token', 'required', 'on' => self::SCENARIO_CONFIRM],
            ['pin', 'required', 'on' => self::SCENARIO_CONFIRM],

        ];
    }
    /**/
    
    public static function findIdentity($id)
    {
        return User::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return User::findOne(['us_access_token' => $token]);
    }

    public function getId()
    {
        return $this->us_id;
    }

    public function getAuthKey()
    {
        return $this->us_access_token;
    }

    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /*
        Вычисляемое поле Username
    */
    public function getUsername()
    {
        return (!empty($this->us_email)) ? $this->us_email : $this->us_phone;
    }

    public function findByUsername($username) 
    {
        $user = User::findOne(['us_phone' => $username]);
        if (is_null($user)) {
            $user = User::findOne(['us_email' => $username]);
        }
        return $user;
    }

    public function validatePassword($password)
    {
        return $this->us_pass === md5($password);
    }

    /* 
        Связь с Subscriptions
    */
    public function getSubscriptions()
    {
        return $this->hasMany(Subscriptions::className(), ['sbs_user_id' => 'us_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'us_email' => Yii::t('app', 'Email'),
            'us_name' => Yii::t('app', 'Имя'),
            'us_phone' => Yii::t('app', 'Мобильный телефон'),
            'pin' => Yii::t('app', 'PIN Код'),
        ];
    }

    public function afterSave( $insert, $changedAttributes ) {
        parent::afterSave($insert, $changedAttributes);
        if ($insert == true) {
            // На модерацию
            RabbitController::sendDispatch([
                'scenario' => 'newuser',
                'user_id' => $this->us_id,
            ]);
        }
    }

}

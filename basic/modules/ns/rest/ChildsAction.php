<?php

namespace ns\rest;

use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\rest\Action;

class ChildsAction extends Action
{
    public function run($id)
    {
        $model = $this->findModel($id);
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }
        return Yii::createObject([
            'class' => ActiveDataProvider::className(),
            'query' => $model->getChildren(),
        ]);
    }

}
<?php

namespace ns\behaviors;

use Yii;
use yii\base\InvalidCallException;
use yii\db\BaseActiveRecord;
use yii\behaviors\TimestampBehavior;

class DatetimeBehavior extends TimestampBehavior
{

    protected function getValue($event)
    {	
        if ($this->value === null) {
            return (new \DateTime)->format('Y-m-d H:i:s');
        }
        return parent::getValue($event);
    }

}

<?php

namespace app\models;

use Yii;
use \ns\behaviors\TreeBehavior;
use \ns\rest\TreeModelTrait;

/**
 * This is the model class for table "tasks_categorys".
 *
 * @property integer $tskct_id
 * @property string $tskct_name
 * @property integer $tskct_ref
 */
class TasksCategorys extends \yii\db\ActiveRecord
{
    use TreeModelTrait;

    public function behaviors() {
        return [
            [
                'class' => TreeBehavior::className(),
                'parentAttribute' => 'tskct_ref',
                'sortable' => [
                    'sortAttribute' => 'tskct_sort',
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tasks_categorys';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tskct_name'], 'string'],
            [['tskct_ref'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tskct_id' => Yii::t('app', 'Tskct ID'),
            'tskct_name' => Yii::t('app', 'Tskct Name'),
            'tskct_ref' => Yii::t('app', 'Tskct Ref'),
        ];
    }


}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "webpush".
 *
 * @property integer $wbp_id
 * @property string $wbp_token
 * @property integer $wbp_user_id
 * @property string $wbp_created
 */
class Webpush extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'webpush';
    }

    public function behaviors()
    {
        return [
            // Дата создания и последнего редактирования записи
            [
                'class' => \ns\behaviors\DatetimeBehavior::className(),
                'createdAtAttribute' => 'wbp_created',
                'updatedAtAttribute' => 'wbp_updated',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['wbp_token'], 'required'],
            [['wbp_user_id'], 'default', 'value'=> Yii::$app->user->identity->us_id],
            [['wbp_token'], 'string', 'max' => 200],
            ['wbp_token', 'unique', 'targetAttribute' => ['wbp_user_id', 'wbp_token'], 'message' => 'Уже есть такой token!'],
            ['wbp_platform', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'wbp_id' => Yii::t('app', 'Wbp ID'),
            'wbp_token' => Yii::t('app', 'Wbp Token'),
            'wbp_user_id' => Yii::t('app', 'Wbp User ID'),
            'wbp_created' => Yii::t('app', 'Wbp Created'),
        ];
    }
}

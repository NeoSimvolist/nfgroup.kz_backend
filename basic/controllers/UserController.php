<?php

namespace app\controllers;

use Yii;
use ns\rest\RestFullController;
use app\models\User;
use yii\helpers\ArrayHelper;

class UserController extends RestFullController
{
    public $modelClass = 'app\models\User';

    public function actionAccess()
    {
        return Yii::$app->user->identity;
    }

    public function prepareRules() {
        $rules = parent::prepareRules();
        $rules['access'] = ['roles' => ['author']];
        return $rules;
    }

}
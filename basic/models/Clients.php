<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Clients".
 *
 * @property integer $cl_id
 * @property integer $cl_fio
 * @property string $cl_mobile
 * @property string $cl_avatar
 * @property string $cl_birthday
 * @property integer $cl_author_id
 * @property string $cl_created
 */
class Clients extends \yii\db\ActiveRecord
{
		use \ns\models\RabbitModelTrait;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['cl_fio', 'required'],
            ['cl_fio', 'string', 'max' => 40],
            ['cl_author_id', 'integer'],
            ['cl_birthday', 'safe'],
            ['cl_mobile', 'string', 'min'=>11, 'max'=>11],
            ['cl_mobile', 'unique', 'message' => Yii::t('app', 'unique_phone')],
            ['cl_avatar', 'safe'],
        ];
    }

    public function behaviors()
    {
        return [
            // Дата создания и последнего редактирования записи
            [
                'class' => \ns\behaviors\DatetimeBehavior::className(),
                'createdAtAttribute' => 'cl_created',
                'updatedAtAttribute' => false,
            ],
            // Автор и редактор записи
            [
                'class' => \yii\behaviors\BlameableBehavior::className(),
                'createdByAttribute' => 'cl_author_id',
                'updatedByAttribute' => false,
            ],
            // JsonFieldBehavior
            [
                'class' => \skobka\jsonField\behaviors\JsonFieldBehavior::className(),
                'dataField' => 'cl_avatar',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cl_id' => Yii::t('app', 'Cl ID'),
            'cl_fio' => Yii::t('app', 'Cl Fio'),
            'cl_mobile' => Yii::t('app', 'Cl Mobile'),
            'cl_avatar' => Yii::t('app', 'Cl Avatar'),
            'cl_birthday' => Yii::t('app', 'Cl Birthday'),
            'cl_author_id' => Yii::t('app', 'Cl Author ID'),
            'cl_created' => Yii::t('app', 'Cl Created'),
        ];
    }
}

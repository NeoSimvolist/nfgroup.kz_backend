<?php

namespace app\controllers;

use Yii;
use ns\rest\RestFullController;

class CountryController extends RestFullController
{
    public $modelClass = 'app\models\Country';
}
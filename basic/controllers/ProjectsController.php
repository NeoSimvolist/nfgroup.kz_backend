<?php

namespace app\controllers;

use Yii;
use ns\rest\RestFullController;

class ProjectsController extends RestFullController
{
    public $modelClass = 'app\models\Projects';
    
    public function prepareRules() {
        $rules = parent::prepareRules();
        $rules['index'] = [
          'allow' => true,
          'roles' => ['guest'],
        ];
        $rules['view'] = [
          'allow' => true,
          'roles' => ['guest'],
        ];
        $rules['create'] = [
          'allow' => true,
          'roles' => ['user'],
        ];
        $rules['test'] = [
          'allow' => true,
          'roles' => ['guest'],
        ];
        return $rules;
    }

    public function actionTest() {
    	return [
    		'can' => Yii::$app->user->can('create'),
    		'user' => Yii::$app->user->identity
    	];
    }
}
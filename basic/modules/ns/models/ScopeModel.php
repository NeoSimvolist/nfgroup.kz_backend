<?php

namespace ns\models;

use Yii;

class ScopeModel extends \yii\db\ActiveRecord
{
    /**
        ScopeQuery - для разделения запросов на Scope-ы/области видимости по принципу организаций
     */
    public static function find()
    {   
        return new ScopeQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->scope_id = 0;
            return true;
        } else {
            return false;
        }
    }

}

<?php

namespace app\controllers\solutions;

use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\rest\Action;
use yii\web\ServerErrorHttpException;
use app\models\Tasks;
use app\commands\RabbitController;

class ScenarioAction extends Action
{
    public function run($id)
    {
        $model = $this->findModel($id);
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }
        $scenario = Yii::$app->request->get('scenario');
        switch ($scenario) {
            case 'best': // Лучший
                $model->sl_status_id = 1; 
                $model->task->tsk_status_id = 3;
                $model->task->tsk_winner_id = $model->sl_author_id; // Ставим победителя
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    if ($model->save() && $model->task->save()) {
                        $transaction->commit();

                        // Есть победитель
                        RabbitController::sendDispatch([
                            'scenario' => 'winner',
                            'task_id' => $model->task->tsk_id,
                            'user_id' => $model->task->tsk_winner_id, // winner id
                        ]);

                        return $model;
                    } else {
                        $transaction->rollback();
                        throw new ServerErrorHttpException('Неизвестная ошибка при сохранении!');
                    }
                } catch (\Exception $e) {
                    $transaction->rollback();
                    throw new ServerErrorHttpException('Неизвестная ошибка при сохранении!');
                }
                break;

            case 'bad': // Плохой/Не в тему
                $model->sl_status_id = 2; 
                $model->save();
                return $model;
                break;
            
            default:
                Yii::$app->getResponse()->setStatusCode(422, 'Data Validation Failed.');
                return [[
                    'field' => 'scenario',
                    'message' => 'Сценарий не найден!',
                ]];
                break;
        }
        
    }
}
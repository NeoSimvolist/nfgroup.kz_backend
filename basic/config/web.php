<?php

use yii\web\Response;
use yii\web\Request;
use ns\rest\ApiErrorHandler;

$params = require(__DIR__ . '/params.php');

/*
    API 
*/
$api = [
    'auth' => [
        'extraPatterns' => [
            'OPTIONS login' => 'options',
            'OPTIONS signup' => 'options',
            'OPTIONS signupconfirm' => 'options',
        ],
    ],
    'user' => [
        'extraPatterns' => [
            'OPTIONS access' => 'options',
            'OPTIONS info' => 'options',
        ],
    ],
    'files' => [],
    'geo' => [
        'extraPatterns' => [
            'OPTIONS info' => 'options',
            'GET info' => 'info',
        ],
    ],
    'country' => [],
    'regions' => [],
    'mysubscriptions' => [],
    'solutions' => [
        'extraPatterns' => [
            'GET {id}/scenario' => 'scenario',
            'OPTIONS {id}/scenario' => 'options',
        ],
    ],
    'tasks' => [
        'extraPatterns' => [
            'GET {id}/solutions' => 'solutions',
            'OPTIONS {id}/solutions' => 'options',
        ],
    ],
    'cities' => [
        'extraPatterns' => [
            'OPTIONS find' => 'options',
        ],
    ],
    'taskscategorys' => [
        'extraPatterns' => [
            'OPTIONS access' => 'options',
            'OPTIONS {id}/childs' => 'options',
            'OPTIONS {id}/append' => 'options',
            'GET {id}/childs' => 'childs',
            'POST {id}/prepend' => 'prepend',
            'POST {id}/append' => 'append',
            'POST {id}/before' => 'before',
            'POST {id}/after' => 'after',
        ],
    ],
    'webpush' => [
        'extraPatterns' => [
            'OPTIONS bytoken' => 'options',
            'GET bytoken' => 'bytoken',
            'DELETE bytoken' => 'bytokendelete',
        ],
    ],
    'projects' => [],
    'clients' => [
        'extraPatterns' => [
            'OPTIONS find' => 'options',
        ],
    ],
    'reviews' => [],
];

function prepareRules($api) {
    $rules = [];
    foreach ($api as $controller => $params) {
        // default
        $rules[] = yii\helpers\ArrayHelper::merge([
            'class' => 'yii\rest\UrlRule', 
            'controller' => [$controller], 
            'pluralize' => false,
        ], $params);
    }
    return $rules;
}



$config = [
    'timezone' => 'Asia/Almaty',
    //'defaultRoute' => 'user',
    'aliases' => [
        '@ns' => '@app/modules/ns',
    ],
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
        /* формат ответа в соответствии с форматом запроса */
        [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
                'application/xml' => Response::FORMAT_XML,
                'text/html' => Response::FORMAT_HTML,
            ],
        ],
    ],
    'language' => 'ru',
    'sourceLanguage' => 'en-US',
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest'],
        ],
        /*
        'errorHandler' => [
            'class' => 'ApiErrorHandler',
            //'errorAction' => 'site/error',
        ],
        */
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'vijl5tDhR4ZgRr2bBrNzcCwP8KPf51X_',
            // Для того чтобы API мог принимать данные в формате JSON: ns
            'parsers' => [
              'application/json' => 'yii\web\JsonParser',
            ],
        ],
        /*
        'response' => [
            'class' => 'yii\web\Response',
            'on beforeSend' => function ($event) {
                $response = $event->sender;
                if ($response->data !== null) {
                    $response->data = [
                        'success' => $response->isSuccessful,
                        'data' => $response->data,
                    ];
                    //$response->statusCode = 200;
                }
            },
        ],
        */
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => false,
            'loginUrl' => null,
            'enableSession' => false, // ns
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.mail.ru',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
                'username' => 'admin@leads.nfgroup.kz',
                'password' => '4BzwA4BzwA',
                'port' => '587', // Port 25 is a very common port too
                'encryption' => 'tls', 
                'streamOptions' => [
                    'ssl' => [
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                    ],
                ],
            ],
            //'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 0 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'categories' => ['api'],
                    'logFile' => '@app/runtime/logs/info.log',
                    'logVars' => [],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            //'enableStrictParsing' => true, // ns
            'showScriptName' => false,
            'rules' => prepareRules($api),
        ],
        'formatter' => [
            'dateFormat' => 'php:Y-m-d',
            'datetimeFormat' => 'php:Y-m-d H:i:s',
            'timeZone' => 'Asia/Almaty',
            'locale' => 'ru',
            'nullDisplay' => '&nbsp;',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            //'currencyCode' => 'EUR',
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    // каталог, где будут располагаться словари
                    'basePath' => '@app/messages',
                    // исходный язык, на котором изначально
                    // написаны фразы в приложении
                    'sourceLanguage' => 'en',
                ],
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;

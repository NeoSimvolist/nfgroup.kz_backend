<?php

namespace app\controllers;

use Yii;
use yii\rest\Serializer;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use ns\models\Scenarios;
use ns\rest\RestController;
use ns\geo\Geo;
use app\models\Cities;

class GeoController extends RestController
{
    public $authorization = false;

	public function actionInfo()
	{
		$geo = new Geo();
		$ip = Yii::$app->request->get('ip');
		if (empty($ip)) $ip = $_SERVER['REMOTE_ADDR'];
        $info = $geo->getCityFull($ip);
        if (isset($info) && isset($info['city'])) {
            return Cities::findOne(['id' => $info['city']['id']]);
        } else {
            return false;
        }
	}

    public function prepareRules() {
        $rules = parent::prepareRules();
        $rules['info'] = [];
        return $rules;
    }

}

<?php

namespace ns\rest;

use yii;
use yii\rest\ActiveController;
use yii\helpers\ArrayHelper;
use ns\rest\RestFullController;

class RestFullTreeController extends RestFullController
{
    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            // root nodes !inherited
            'index' => [
                'class' => 'ns\rest\IndexAction',
                'modelClass' => $this->modelClass,
                //'checkAccess' => [$this, 'checkAccess'],
            ],
            // delete !inherited
            'delete' => [
                'class' => 'ns\rest\DeleteAction',
                'modelClass' => $this->modelClass,
                //'checkAccess' => [$this, 'checkAccess'],
            ],
            // child nodes
            'childs' => [
                'class' => 'ns\rest\ChildsAction',
                'modelClass' => $this->modelClass,
                //'checkAccess' => [$this, 'checkAccess'],
            ],
            // append node
            'append' => [
                'class' => 'ns\rest\CreateAction',
                'modelClass' => $this->modelClass,
                //'checkAccess' => [$this, 'checkAccess'],
            ],
            // prepend node
            'prepend' => [
                'class' => 'ns\rest\CreateAction',
                'modelClass' => $this->modelClass,
                //'checkAccess' => [$this, 'checkAccess'],
            ],
            // before node
            'before' => [
                'class' => 'ns\rest\CreateAction',
                'modelClass' => $this->modelClass,
                //'checkAccess' => [$this, 'checkAccess'],
            ],
            // after node
            'after' => [
                'class' => 'ns\rest\CreateAction',
                'modelClass' => $this->modelClass,
                //'checkAccess' => [$this, 'checkAccess'],
            ],

        ]);
    }
}
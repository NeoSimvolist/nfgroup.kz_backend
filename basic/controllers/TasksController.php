<?php

namespace app\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use ns\rest\RestFullController;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

/* 
	Все Заявки
*/
class TasksController extends RestFullController
{
    public $modelClass = 'app\models\Tasks';

    public function actions()
    {
        return \yii\helpers\ArrayHelper::merge(parent::actions(), [
            // solutions nodes
            'solutions' => [
                'class' => 'app\controllers\tasks\SolutionsAction',
                'modelClass' => $this->modelClass,
            ],
            // delete
            'delete' => [
                'class' => 'app\controllers\tasks\DeleteAction',
                'modelClass' => $this->modelClass,
            ],
        ]);
    }

    public function prepareRules() {
        $rules = parent::prepareRules();
        $rules['solutions'] = ['roles' => ['author']];
        $rules['update'] = ['roles' => ['moderation']];
        $rules['delete'] = ['roles' => ['author']];
        return $rules;
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        switch ($action) {
            case 'delete':
                if (!(
                    (Yii::$app->user->can('isAuthor', ['id' => $model->tsk_author_id]) && $model->tsk_status_id !== 3) || 
                    Yii::$app->user->can('delete')
                )) {
                    throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
                }
                break;
            
            default:
                # code...
                break;
        }
    }

}
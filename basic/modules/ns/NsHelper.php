<?php

	namespace ns;

	use Yii;

	// Получаем допустимый размер загружаемого файла, исходя из настроек сервера
	function file_upload_max_size() {
	  $max_size = -1;
	  if ($max_size < 0) {
	    $max_size = parse_size(ini_get('post_max_size'));
	    $upload_max = parse_size(ini_get('upload_max_filesize'));
	    if ($upload_max > 0 && $upload_max < $max_size) {
	      $max_size = $upload_max;
	    }
	  }
	  return $max_size;
	}

	/**
	* 
	*/
	class NsHelper
	{
		
		static function is_json($string, $return_data = false) {
			$data = json_decode($string);
			return (json_last_error() == JSON_ERROR_NONE) ? ($return_data ? $data : TRUE) : FALSE;
		}

		static function json_decode($string, $default = false) {
			$data = json_decode($string, true);
			return (json_last_error() == JSON_ERROR_NONE) ? $data : $default;
		}

		// Загрузка файлов
		static function upload($field = null, $params = []) {
			$r = ['field' => $field];
		    if (empty($_FILES)) {
		    	$r['error'] = 'Ниодного файла не получено';
		    	return $r;	
		    }
		    if (is_null($field)) {
		    	$keys = array_keys($_FILES);
		    	$aFile = $_FILES[$keys[0]];
		    	$r['field'] = $keys[0];
		    } else {
		    	if (!array_key_exists($field, $_FILES)) {
			    	$r['error'] = '$_FILES["'.$field.'"] нет данных';
			    	return $r;	
		    	}
		    	$aFile = $_FILES[$field];	
		    }

		    if (!isset($params['uploadDir']) || empty($params['uploadDir'])) {
		    	$r['error'] = 'Директория для загрузки не указана: uploadDir';
		    	return $r;	
		    }

		    if ($aFile['error'] !== UPLOAD_ERR_OK) {
		        if ($aFile['error'] == UPLOAD_ERR_INI_SIZE) {
			    	$r['error'] = 'Превышен допустимый размер файла: '. round(file_upload_max_size()/1024/1024) . ' МБ';
			    	return $r;
		        } else {
			    	$r['error'] = 'Ошибка при загрузке файла! '.$aFile['error'];
			    	return $r;
		        }
		    } else {
		        $sExt = pathinfo($aFile['name'],PATHINFO_EXTENSION);
		        $r['extension'] = strtolower($sExt);
		        $r['name'] = $aFile['name'];
		        $aExtensions = isset($params['extensions']) ? $params['extensions'] : array();
		        if (!in_array(strtolower($sExt), $aExtensions) && !empty($aExtensions)) {
		        	unlink($aFile['tmp_name']);
			    	$r['error'] = 'Разрешенные форматы файлов: ' . implode(', ', $aExtensions);
			    	return $r;
		        }
		        $sFileName = $params['uploadDir'].'/'.uniqid().'.'.$sExt;
		        if (move_uploaded_file($aFile['tmp_name'], $sFileName)) {
		        	$r['size'] = filesize($sFileName);
		        	$r['filename'] = $sFileName;
		            return $r;
		        } else {
		            // Удаляем временный файл
		            unlink($aFile['tmp_name']);
			    	$r['error'] = 'Ошибка при сохранении файла!';
			    	return $r;
		        }
		    }
		}

	}



?>
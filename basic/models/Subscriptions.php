<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "subscriptions".
 *
 * @property integer $sbs_id
 * @property integer $sbs_user_id
 * @property integer $sbs_category_id
 */
class Subscriptions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscriptions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sbs_user_id', 'sbs_category_id'], 'required'],
            [['sbs_user_id', 'sbs_category_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sbs_id' => Yii::t('app', 'Sbs ID'),
            'sbs_user_id' => Yii::t('app', 'Sbs User ID'),
            'sbs_category_id' => Yii::t('app', 'Sbs Category ID'),
        ];
    }
}

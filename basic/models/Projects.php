<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "projects".
 *
 * @property integer $prj_id
 * @property string $prj_name
 * @property string $prj_created
 * @property string $prj_updated
 * @property string $prj_body
 * @property string $prj_preview
 * @property integer $prj_author_id
 */
class Projects extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'projects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prj_name', 'prj_body', 'prj_preview'], 'required'],
            [['prj_created', 'prj_updated'], 'safe'],
            [['prj_body'], 'string'],
            [['prj_author_id'], 'integer'],
            ['prj_author_id', 'default', 'value'=> Yii::$app->user->identity->us_id],
            [['prj_name', 'prj_preview'], 'string', 'max' => 255],
        ];
    }

    public function behaviors()
    {
        return [
            // Дата создания и последнего редактирования записи
            [
                'class' => \ns\behaviors\DatetimeBehavior::className(),
                'createdAtAttribute' => 'prj_created',
                'updatedAtAttribute' => 'prj_updated',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'prj_id' => Yii::t('app', 'Prj ID'),
            'prj_name' => Yii::t('app', 'Prj Name'),
            'prj_created' => Yii::t('app', 'Prj Created'),
            'prj_updated' => Yii::t('app', 'Prj Updated'),
            'prj_body' => Yii::t('app', 'Prj Body'),
            'prj_preview' => Yii::t('app', 'Prj Preview'),
            'prj_author_id' => Yii::t('app', 'Prj Author ID'),
        ];
    }
}

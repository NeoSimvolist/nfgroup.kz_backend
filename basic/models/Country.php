<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sxgeo_country".
 *
 * @property integer $id
 * @property string $iso
 * @property string $continent
 * @property string $name_ru
 * @property string $name_en
 * @property string $lat
 * @property string $lon
 * @property string $timezone
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sxgeo_country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'iso', 'continent', 'name_ru', 'name_en', 'lat', 'lon', 'timezone'], 'required'],
            [['id'], 'integer'],
            [['lat', 'lon'], 'number'],
            [['iso', 'continent'], 'string', 'max' => 2],
            [['name_ru', 'name_en'], 'string', 'max' => 128],
            [['timezone'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'iso' => Yii::t('app', 'Iso'),
            'continent' => Yii::t('app', 'Continent'),
            'name_ru' => Yii::t('app', 'Name Ru'),
            'name_en' => Yii::t('app', 'Name En'),
            'lat' => Yii::t('app', 'Lat'),
            'lon' => Yii::t('app', 'Lon'),
            'timezone' => Yii::t('app', 'Timezone'),
        ];
    }
}

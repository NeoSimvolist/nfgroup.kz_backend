<?php

namespace app\controllers;

use Yii;
use ns\rest\RestFullController;

/* 
	Категории на которые подписан пользователь
*/
class MysubscriptionsController extends RestFullController
{
    public $modelClass = 'app\models\MySubscriptions';

    public function prepareRules() {
        $rules = parent::prepareRules();
        $rules['delete'] = ['roles' => ['author']];
        return $rules;
    }

}
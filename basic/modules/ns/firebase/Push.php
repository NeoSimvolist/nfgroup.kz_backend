<?php

namespace ns\firebase;

class Push
{
	// $notification: body, title, click_action, icon
	public static function Send($ids, $notification, $data = null) {
		try {
		    $url = 'https://fcm.googleapis.com/fcm/send';
		    $fields = [
		        //'to' => $id,
		        'registration_ids' => $ids,
		    ];
		    if (!is_null($notification)) $fields['notification'] = $notification;
		    if (!is_null($data)) $fields['data'] = $data;
		    $fields = json_encode( $fields );
		    $headers = [
		        'Authorization: key=AAAAZ58nB-8:APA91bHhyLtfxWAexFgdR2QtD_dfrCRxM0EbOxUfZ1YkyCw5jvIP19G2Q9eK6Wwk_yw5V7wPk2ZjQD-2QlRR5AYCQLXVxoJMNoryyasmESv2jbNRZsJDhf9ZYrJzI0PDQuw_3rxh8bXk',
		        'Content-Type: application/json',
		    ];
		    $ch = curl_init();
		    curl_setopt ( $ch, CURLOPT_URL, $url );
		    curl_setopt ( $ch, CURLOPT_POST, true );
		    curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		    curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

		    $result = curl_exec ( $ch );
		    curl_close ( $ch );
		    return json_decode($result, true);
		} catch (Exception $e) {
		    return false;
		} 
	}
}
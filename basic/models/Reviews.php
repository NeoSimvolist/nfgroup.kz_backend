<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reviews".
 *
 * @property integer $rw_id
 * @property string $rw_message
 * @property integer $rw_author_id
 * @property string $rw_created
 */
class Reviews extends \yii\db\ActiveRecord
{

		use \ns\models\RabbitModelTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reviews';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rw_message'], 'required'],
            [['rw_message'], 'string'],
            [['rw_author_id'], 'integer'],
            [['rw_created'], 'safe'],
        ];
    }

    public function behaviors()
    {
        return [
            // Дата создания и последнего редактирования записи
            [
                'class' => \ns\behaviors\DatetimeBehavior::className(),
                'createdAtAttribute' => 'rw_created',
                'updatedAtAttribute' => false,
            ],
            // Автор и редактор записи
            [
                'class' => \yii\behaviors\BlameableBehavior::className(),
                'createdByAttribute' => 'rw_author_id',
                'updatedByAttribute' => false,
            ],
        ];
    }

    public function fields()
    {
        $fields = parent::fields();
        // Добавляем поля в выдачу
        $fields['rw_author_name'] = function() {
            if (!isset($this->author)) return null;
            return $this->author->us_name;
        };
        return $fields;
    }

    /* 
        Связь с Users / Автор
    */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['us_id' => 'rw_author_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rw_id' => Yii::t('app', 'Rw ID'),
            'rw_message' => Yii::t('app', 'Rw Message'),
            'rw_author_id' => Yii::t('app', 'Rw Author ID'),
            'rw_created' => Yii::t('app', 'Rw Created'),
        ];
    }
}

<?php

namespace ns\behaviors;

// implemented as Serialize 
class JsonBehavior extends ConverterBehavior
{
    protected function convertToStoredFormat($value)
    {
        return $value;
    }

    protected function convertFromStoredFormat($value)
    {
        return $value;
    }
}
<?php

namespace app\controllers;

use Yii;
use ns\rest\RestFullController;

class ClientsController extends RestFullController
{
    public $modelClass = 'app\models\Clients';
    
    public function prepareRules() {
        $rules = parent::prepareRules();
        $rules['index'] = [
          'allow' => true,
          'roles' => ['guest'],
        ];
        $rules['view'] = [
          'allow' => true,
          'roles' => ['guest'],
        ];
        $rules['find'] = [
          'allow' => true,
          'roles' => ['guest'],
        ];
        $rules['create'] = [
          'allow' => true,
          'roles' => ['guest'],
        ];
        $rules['update'] = [
          'allow' => true,
          'roles' => ['guest'],
        ];
        return $rules;
    }

    public function actionFind()
    {
    	$class = $this->modelClass;
    	$search = Yii::$app->request->get('search');
    	if (empty($search)) {
    		$query = $class::find();
    	} else {
    		$query = $class::find()
    			->andWhere(['like', 'cl_fio', $search])
    			->orWhere(['like', 'cl_mobile', $search]);
    	}
			return new \yii\data\ActiveDataProvider([
			  'query' => $query,
			]);
    }

}
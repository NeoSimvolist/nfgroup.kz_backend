<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tasks_status".
 *
 * @property integer $tskst_id
 * @property string $tskst_name
 */
class TasksStatus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tasks_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tskst_name'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tskst_id' => Yii::t('app', 'Tskst ID'),
            'tskst_name' => Yii::t('app', 'Tskst Name'),
        ];
    }
}
